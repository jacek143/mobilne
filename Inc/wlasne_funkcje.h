/**
 * @file wlasne_funkcje.h
 *
 *  @date 23.052015
 *
 *  @author Jacek Jankowski
 *
 *  @brief Plik, w kt�rym znajduj� si� deklaracje naszych w�asnych funkcji.
 */

#ifndef WLASNE_FUNKCJE_H_
#define WLASNE_FUNKCJE_H_

#include "stm32f3xx_hal.h"
#include "cmsis_os.h"

#define ABS(x) ((x) >= 0 ? (x) : -(x))

typedef enum dioda_t{
	ir1, ir2, ir3, ir4, ir5, ir6
} dioda_t;

extern int Predkosc;

void init(void);
void silniki(int8_t lewy, int8_t prawy);
void KontrolaSilnikowTask(void const * argument);
void PomiaryADCTask(void const * argument);
int ZwrocLewyPredkosc(void);
int ZwrocPrawyPredkosc(void);
int ZwrocLewyZadanaPredkosc(void);
int ZwrocPrawyZadanaPredkosc(void);
void ZadajPredkosc(int Lewy, int Prawy);
void WykonajObrot(int Kat);
void WykonajPrzemieszczenie(int Przemieszczenie);
uint32_t ZwrocADCIR(dioda_t Czujnik);
void Wyrownaj(void);

#endif /* WLASNE_FUNKCJE_H_ */
