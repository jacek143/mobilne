/**
 * @file wlasne_funkcje.c
 *
 *  @date 23.052015
 *
 *  @author Jacek Jankowski
 *
 *  @brief Plik, w kt�rym znajduj� si� definicje naszych w�asnych funkcji.
 */

#include "wlasne_funkcje.h"

#define ABS(x) ((x) >= 0 ? (x) : -(x))

extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim4;
extern ADC_HandleTypeDef hadc1;

/**
 * @brief Predko��, z kt�r� ma si� przemieszcza� robot.
 */
int Predkosc = 5;

/**
 * @brief Predkosc lewego ko�a.
 */
int LewyPredkosc;

/**
 * @brief Predko�� prawego ko�a.
 */
int PrawyPredkosc;

/**
 * @brief ��dana predko�� lewego ko�a.
 */
int LewyZadanaPredkosc;

/**
 * @brief ��dana predko�� prawego ko�a.
 */
int PrawyZadanaPredkosc;

/**
 * @brief Pomiary ADC przekazywane od razu z przetwornika prez DMA.
 */
uint32_t ADC_DMA[6];

/**
 * @brief Pomiary wykonane na fototranzystorach, kiedy odpowiednia dioda jest zapalona.
 */
uint32_t ADC_IR[6];

int CzyJestScianaPrzednia(void){
	volatile uint32_t tmp1 = ZwrocADCIR(ir4);
	volatile uint32_t tmp2 = ZwrocADCIR(ir5);

	if(tmp1 > 813 && tmp2 > 2700){
		return 1;
	}
	else{
		return 0;
	}
}

/**
 * @brief Wyr�wnuje robota wzgl�dem �cianek.
 */
void Wyrownaj(void) {
	const int32_t offset = 10;
	const uint32_t granica = 190;
	int lewy, prawy, warunek, l, p;
	warunek = 1;
	while(warunek){
		warunek = 0;
		l = ZwrocADCIR(ir3)+ offset;
		p = ZwrocADCIR(ir4);
		if (l < granica){
			lewy = -1;
		}
		else lewy = 1;
		if(p < granica) {
			prawy = -1;
		}
		else prawy = 1;

		if(((granica - 1) >= l) || (l >= (granica + 1)) || ((granica - 1) >= p) || (p >= (granica + 1))){
			ZadajPredkosc(lewy, prawy);
			warunek = 1;
		}
		osDelay(1);
	}
	/////////////
//	while ((ZwrocADCIR(ir4) + offset) < granica && ZwrocADCIR(ir5) < granica) {
//		ZadajPredkosc(-1, -1);
//	}
//
//	while ((ZwrocADCIR(ir4) + offset) > granica && ZwrocADCIR(ir5) > granica) {
//		ZadajPredkosc(1, 1);
//	}
//
//	while ((ZwrocADCIR(ir4) + offset) > ZwrocADCIR(ir5)) {
//		ZadajPredkosc(1, -1);
//	}
//
//	while ((ZwrocADCIR(ir4) + offset) < ZwrocADCIR(ir5)) {
//		ZadajPredkosc(-1, 1);
//	}

	ZadajPredkosc(0, 0);

//
//		while ((ZwrocADCIR(ir4) + offset) < granica && ZwrocADCIR(ir5) < granica) {
//			ZadajPredkosc(-1, -1);
//		}
//
//		while ((ZwrocADCIR(ir4) + offset) > granica && ZwrocADCIR(ir5) > granica) {
//			ZadajPredkosc(1, 1);
//		}
//
//		while ((ZwrocADCIR(ir4) + offset) > ZwrocADCIR(ir5)) {
//			ZadajPredkosc(1, -1);
//		}
//
//		while ((ZwrocADCIR(ir4) + offset) < ZwrocADCIR(ir5)) {
//			ZadajPredkosc(-1, 1);
//		}
//
//		ZadajPredkosc(0, 0);
}

/**
 * @brief Zwraca odpowiedni indeks w tablicy ADC dla podanej diody.
 *
 * @param Czujnik - Dioda, kt�r� chcemy wykorzysta� do pomiaru.
 *
 * @retval Odpowiedni indeks w tablicy ADC dla podanej diody.
 */
unsigned ZwrocIndeksADC(dioda_t Czujnik) {
	switch (Czujnik) {
	case ir1:
		return 5;
	case ir2:
		return 4;
	case ir3:
		return 3;
	case ir4:
		return 2;
	case ir5:
		return 1;
	default: // ir6
		return 0;
	}
}

/**
 * @brief Dokonuje pomiaru odleg�o�ci za pomoc� pomiaru IR.
 *
 * @brief Czujnik - Czujnik, dla kt�rego ma zosta� wykonany pomiar.
 */
uint32_t ZwrocADCIR(dioda_t Czujnik) {
	return ADC_IR[Czujnik];
}

/**
 * @brief Inicjalizacja u�ywanych peryferi�w.
 */
void init(void){
	HAL_TIM_Base_Start(&htim4);
	HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_2);

	HAL_TIM_Base_Start(&htim3);
	HAL_TIM_Encoder_Start(&htim2, TIM_CHANNEL_1);
	HAL_TIM_Encoder_Start(&htim2, TIM_CHANNEL_2);

	HAL_TIM_Base_Start(&htim3);
	HAL_TIM_Encoder_Start(&htim3, TIM_CHANNEL_1);
	HAL_TIM_Encoder_Start(&htim3, TIM_CHANNEL_2);

	HAL_ADC_Start_DMA(&hadc1, ADC_DMA, 6);
}

/**
 * @brief Regulacja obrot�w silnik�w w procentach.
 *
 * @param lewy - Procentowe wype�nienie na lewym silniku.
 *
 * @param prawy - Procentowe wype�nienie na prawym silniku.
 */
void silniki(int8_t lewy, int8_t prawy){

	if(lewy > 0){
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_10, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_8, GPIO_PIN_SET);
	}
	else if (lewy < 0){
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_10, GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_8, GPIO_PIN_RESET);
	}

	if(prawy > 0){
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_11, GPIO_PIN_SET);
	}
	else if (prawy < 0){
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_11, GPIO_PIN_RESET);
	}

	TIM4->CCR1 = lewy > 0 ? lewy : -lewy;
	TIM4->CCR2 = prawy > 0 ? prawy : -prawy;
}



/**
 * @brief Task opowiadaj�cy za pomiary adc.
 *
 * @param - Nic nie przekazujemy. Argument w deklaracji jest wymagany
 * ze wzgl�d�w formalnych RTOS.
 */
void PomiaryADCTask(void const * argument) {
	uint16_t GPIO_Pin;
	dioda_t Czujnik;
	while (1) {
		for (Czujnik = ir1; Czujnik <= ir6; Czujnik++) {
			switch (Czujnik) {
			case ir1:
				GPIO_Pin = GPIO_PIN_3;
				break;
			case ir2:
				GPIO_Pin = GPIO_PIN_4;
				break;
			case ir3:
				GPIO_Pin = GPIO_PIN_5;
				break;
			case ir4:
				GPIO_Pin = GPIO_PIN_0;
				break;
			case ir5:
				GPIO_Pin = GPIO_PIN_1;
				break;
			default:// ir6:
				GPIO_Pin = GPIO_PIN_2;
				break;
			} // switch
			HAL_GPIO_WritePin(GPIOD, GPIO_Pin, GPIO_PIN_SET);
			osDelay(5);
			HAL_GPIO_WritePin(GPIOD, GPIO_Pin, GPIO_PIN_RESET);
			ADC_IR[Czujnik] = ADC_DMA[ZwrocIndeksADC(Czujnik)];
		} // for
	} // while
}

/**
 * @brief Task odpowiadaj�cy za kontrolowanie silnik�w.
 */
void KontrolaSilnikowTask(void const * argument) {
	uint16_t PoprzLewyLicznik = 0, PoprzPrawyLicznik = 0;
	uint16_t LewyLicznik = 0, PrawyLicznik = 0;

	const int Kp = 1, Ki = 1;

	int LewyBlad = 0, PrawyBlad = 0, LewySumaBledu = 0, PrawySumaBledu = 0;


	for (;;) {
		LewyLicznik = TIM3->CNT;
		PrawyLicznik = TIM2->CNT;

		LewyPredkosc = LewyLicznik - PoprzLewyLicznik;
		PoprzLewyLicznik = LewyLicznik;

		PrawyPredkosc = PrawyLicznik - PoprzPrawyLicznik;
		PoprzPrawyLicznik = PrawyLicznik;

		LewyBlad = ZwrocLewyZadanaPredkosc() - ZwrocLewyPredkosc();
		PrawyBlad = ZwrocPrawyZadanaPredkosc() - ZwrocPrawyPredkosc();

		if(LewyBlad > 0){
			LewySumaBledu++;
		}
		else if(LewyBlad < 0){
			LewySumaBledu--;
		}

		if(PrawyBlad > 0){
			PrawySumaBledu++;
		}
		else if(PrawyBlad < 0){
			PrawySumaBledu--;
		}

		silniki(Kp * LewyBlad + Ki * LewySumaBledu, Kp * PrawyBlad + Ki * PrawySumaBledu);

		osDelay(10);
	}
}

/**
 * @brief Zwraca aktualn� predko�� lewego ko�a.
 */
int ZwrocLewyPredkosc(void){
	return LewyPredkosc;
}

/**
 * @brief Zwraca aktualn� predko�� prawego ko�a.
 */
int ZwrocPrawyPredkosc(void){
	return PrawyPredkosc;
}

/**
 * @brief Zwraca predko�� zadan� dla lewego ko�a.
 */
int ZwrocLewyZadanaPredkosc(void){
	return LewyZadanaPredkosc;
}

/**
 * @brief Zwraca predko�� zadan� dla prawego ko�a.
 */
int ZwrocPrawyZadanaPredkosc(void){
	return PrawyZadanaPredkosc;
}

/**
 * @brief Zadawanie pr�dko�ci k�.
 *
 * @param Lewy - Pr�dko�� dla lewego ko�a.
 *
 * @param Prawy - Predko�� dla prawego ko�a.
 */
void ZadajPredkosc(int Lewy, int Prawy){
	LewyZadanaPredkosc = Lewy;
	PrawyZadanaPredkosc = Prawy;
}

/**
 * @brief Wykonuje ob�rt o zadany kat.
 *
 * @param Kat - Zadany k�t obrotu.
 */
void WykonajObrot(int Kat){
	const float t = 1.148 * (ABS(Kat) * Predkosc);
	if(Kat > 0){
		ZadajPredkosc(-Predkosc,Predkosc);
	}
	else{
		ZadajPredkosc(Predkosc,-Predkosc);
	}
	osDelay(t);
	ZadajPredkosc(0,0);
	osDelay(300);
}

/**
 * @brief Wykonuje przemieszczenie w lini prostej.
 *
 * @param Przemieszczenie - Przemieszczenie zadane w mm.
 */
void WykonajPrzemieszczenie(int Przemieszczenie){
	const float t = 1.3 * (ABS(Przemieszczenie) * Predkosc);
	if(Przemieszczenie > 0){
		ZadajPredkosc(Predkosc, Predkosc);
	}
	else{
		ZadajPredkosc(-Predkosc, - Predkosc);
	}
	osDelay(t);
	ZadajPredkosc(0,0);
	osDelay(300);
}
